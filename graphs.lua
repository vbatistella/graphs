Graphs = {}

function Graphs:new (nnodes, o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.nnodes = nnodes
	o.nedges = 0
	o.edges = {}
	o.nodes = {}
	for i = 1, nnodes do
		o.edges[i] = {}
		o.nodes[i] = 0
		for j = 1, nnodes do
			o.edges[i][j] = false
		end
	end
	return o
end

-- Add an edge
function Graphs:add_edge(i, j)
	self.edges[i][j] = true
	self.edges[j][i] = true
	self.nedges = self.nedges + 1
end

-- Remove an edge
function Graphs:remove_edge(i, j)
	self.edges[i][j] = false
	self.edges[j][i] = false
	self.nedges = self.nedges - 1
end

-- Returns the genus of the graph
function Graphs:genus()
	return self.nedges - self.nnodes + 1
end

-- Returns a list of adjacent nodes
function Graphs:adjacent(i)
	local count = 1
	local adjacent = {}
	for j = 1, self.nnodes do
		if self.edges[i][j] then
			adjacent[count] = j
			count = count + 1
		end
	end
	return adjacent
end

-- Give value to a node
function Graphs:add_value(i, v)
	self.nodes[i] = v
end

-- Returns the sum of the nodes values
function Graphs:total_value()
	local total = 0
	for i = 1, self.nnodes do
		total = total + self.nodes[i]
	end
	return total
end

-- Check if the game is valid and if it's for sure winnable
function Graphs:is_valid()
	for i = 1, self.nnodes do
		if self.nodes[i] == nil then
			return false
		end
	end

	if self:total_value() < self:genus() then
		return false
	else
		return true
	end
end

-- Steals adjacents points
function Graphs:push(i)
	local a = self:adjacent(i)
	local n = #a

	self.nodes[i] = self.nodes[i] - n

	for j = 1, n do
		self.nodes[a[j]] = self.nodes[a[j]] + 1
	end
end

-- Give the node points to its adjacents
function Graphs:pull(i)
	local a = self:adjacent(i)
	local n = #a

	self.nodes[i] = self.nodes[i] + n

	for j = 1, n do
		self.nodes[a[j]] = self.nodes[a[j]] - 1
	end
end

-- Check if the game is won
function Graphs:is_won()
	for i = 1, self.nnodes do
		if self.nodes[i] < 0 then
			return false
		end
	end
	return true
end

function Graphs:get_locations(cx, cy, r)
	local locations = {}
	local step = 2*math.pi/self.nnodes
	for i = 1, self.nnodes do
		locations[i] = {}
		locations[i][1] = cx+r*math.cos(step*i)
		locations[i][2] = cy+r*math.sin(step*i)
	end
	return locations
end

