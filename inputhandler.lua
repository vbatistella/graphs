local inputhandler = {}

Inputhandler = {x = 0, y = 0, tolerance = 10, node = 0}

function Inputhandler:new (o, x, y)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
  self.x = x
  self.y = y
  self.tolerance = 10
	return o
end

-- Calculates distance between two points
function distance(x1, y1, x2, y2)
  return math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))
end

-- Returns 1 if input moved upwards and -1 if downwards, and 0 if not moved
function Inputhandler:check_y(x, y)
  if distance(x,y,self.x,self.y) >= self.tolerance then
    if y < self.y then
      return 1
    else
      return -1
    end
  else
    return 0
  end
end

-- Returns node in some position
function Inputhandler:in_node(x, y, locations, node_size)
  for i = 1, #locations do
    if distance(x, y, locations[i][1], locations[i][2]) <= node_size then
      return i
    end
  end
  return 0
end
