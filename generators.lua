require "graphs"
Generators = {}

function Generators.connectGraph(g, nnodes)
	local queue = {1}
	local first=1 
	local last=1
	local reacheds = {true}
	local snr = 2
	while(#reacheds < nnodes) do
		while(last >= first) do
			local node = queue[first]
			first = first + 1
			for _, i in ipairs(g:adjacent(node)) do
				if(reacheds[i] ~= true) then
					reacheds[i] = true
					last = last + 1
					queue[last] = i
				end
			end
		end
		while(reacheds[snr]) do
			snr = snr + 1
		end
		if(snr <= nnodes) then
			local r = math.random(1, #reacheds)
			g:add_edge(queue[r],snr)
			last = last + 1
			queue[last] = snr
			reacheds[snr] = true
			snr = snr + 1
		end
	end
end

function Generators.randomValues(g, nnodes)
	local genus = g:genus()
	local current = 0
	for i=1, nnodes-1 do
		value = math.random(-nnodes, nnodes)
		current = current + value
		g:add_value(i, value)
	end
	g:add_value(nnodes, genus - current)
end

function Generators.randomGraph(nnodes, prob)
	prob = prob or 50
	local g = Graphs:new(nnodes)
	for i=2, nnodes do
		g:add_edge(math.random(1,i-1), i)
	end
	for i=1, nnodes do
		for j=i+1, nnodes do
			if(math.random(1, 100) < prob) then
				g:add_edge(i,j)
			end
		end
	end
	Generators.randomValues(g, nnodes)
	return g 
end

return Generators