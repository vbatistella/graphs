require "graphs"
require "generators"
require "printer"
require "inputhandler"

local font_size
local node_size
local radius
local graph_location
local little_menu_input
local little_menu
local width
local height 
local rand
local rand2
local graph_location 

function test()
	local g = Graphs:new(nil,3)
	g:add_edge(1,2)
	g:add_edge(2,3)
	g:add_edge(3,1)

	g:add_value(1,4)
	g:add_value(2,2)
	g:add_value(3,-2)

	print("is valid?:",g:is_valid())
	print("total value:",g:total_value())

	local a = g:adjacent(1)

	print("adjacent to node 1:")
	for i = 1, #a do
		print(a[1])
	end

	print("won?:",g:is_won())
	print("push!")
	g:push(1)
	print("won?:",g:is_won())
	print("pull!")
	g:pull(3)
	print("won?:",g:is_won())
	print("total value:",g:total_value())
end


function love.load()
	math.randomseed(love.timer.getTime())
	font_size = 1.5
	node_size = 50
	radius = 200
	graph_location = {}
	little_menu_input = true
	little_menu = false
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()
	rand = Generators.randomGraph(5)
	rand2 = Generators.randomGraph(6)
	graph_location = rand:get_locations(width/2, height/2, radius)
	
	-- test()
end

function love.mousepressed(x, y, button, istouch)
	ih = Inputhandler:new(nil,x,y)
	ih.node = ih:in_node(x,y,graph_location,node_size)

	-- Flag to print little menu
	if little_menu_input then
		little_menu = true
	end
end

function love.mousereleased(x, y, button, istouch, presses)
	-- little menu input implementation
	if little_menu_input then
		local result = ih:check_y(x,y)
		little_menu = false
		if ih.node ~= 0 then
			if result == 1 then
				rand:push(ih.node)
			end
			if result == -1 then
				rand:pull(ih.node)
			end
		end
	end

	-- Check if won
	if rand:is_won() then
		print("Woo, you won!")
	end
end

function love.update(dt)
end

function love.draw()
    love.graphics.setColor(0, 0.4, 0.4)
		-- print graph
		print_graph(rand, graph_location, node_size)
		-- print little menu
		if little_menu_input then
			if little_menu then
				print_little_menu(ih)
			end
		end
end
