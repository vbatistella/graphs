
-- print edges
function print_edges(g, locations, node_size)
	local n = g.nnodes
	local step = 2*math.pi/n

	for i = 1, n do
		for j = i+1, n do
			if g.edges[i][j] then
				local x1 = locations[i][1]
				local y1 = locations[i][2]
				local x2 = locations[j][1]
				local y2 = locations[j][2]

				x1 = x1+((x2-x1)*node_size)/math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))
				y1 = y1+((y2-y1)*node_size)/math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))
				x2 = x2-((x2-x1)*node_size)/math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))
				y2 = y2-((y2-y1)*node_size)/math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))
				love.graphics.line( x1, y1, x2, y2)
			end
		end
	end
end

function print_graph(g, locations, node_size)
	print_nodes(g, locations, node_size)
	print_edges(g, locations, node_size)
end

-- print nodes
function print_nodes(g, locations, node_size)
	for i = 1, g.nnodes do
		if g.nodes[i] < 0 then
			love.graphics.setColor(1, 0, 0)
		else
			love.graphics.setColor(0, 0.4, 0.4)
		end
		love.graphics.circle("line", locations[i][1], locations[i][2], node_size)
		love.graphics.print( g.nodes[i], locations[i][1], locations[i][2], 0, font_size, font_size, 0, 0, 0, 0 )
	end
end

-- print little menu
function print_little_menu(input)
	love.graphics.setColor(1, 1, 1)
	x,y = love.mouse.getPosition()
	-- print the x
	if input:check_y(x,y) == 0 then
		love.graphics.setColor(1, 0, 0)
	end
	little_menu_x(input)
	love.graphics.setColor(1, 1, 1)
	-- print words
	it = input.tolerance
	local dy = 2*it
	-- change color
	if input:check_y(x,y) == 1 then
		love.graphics.setColor(1, 0, 0)
	end
	love.graphics.printf("Push!", input.x-2*it, input.y-it-dy, it*4, "center",0,1,1)
	love.graphics.setColor(1, 1, 1)
	if input:check_y(x,y) == -1 then
		love.graphics.setColor(1, 0, 0)
	end
	love.graphics.printf("Pull!", input.x-2*it, input.y-it+dy, it*4, "center",0,1,1)
	love.graphics.setColor(1, 1, 1)
end
-- print the x in the little menu
function little_menu_x(input)
	love.graphics.circle("line", input.x, input.y, input.tolerance)
	local x1 = input.x+math.cos(math.pi/4)*input.tolerance
	local y1 = input.y+math.sin(math.pi/4)*input.tolerance
	local x2 = input.x-math.cos(math.pi/4)*input.tolerance
	local y2 = input.y-math.sin(math.pi/4)*input.tolerance
	love.graphics.line(x1, y1, x2, y2)
	local x1 = input.x-math.cos(3*math.pi/4)*input.tolerance
	local y1 = input.y-math.sin(3*math.pi/4)*input.tolerance
	local x2 = input.x+math.cos(3*math.pi/4)*input.tolerance
	local y2 = input.y+math.sin(3*math.pi/4)*input.tolerance
	love.graphics.line(x1, y1, x2, y2)
end
